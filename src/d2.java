import java.util.Arrays;
public class d2 {
    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 3};
        int n = a.length;

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i] == a[j]) {
                    a[j] = a[n - 1];
                    n--;
                    j--;
                }
            }
        }

        int[] b = Arrays.copyOf(a, n);

        for (int p = 0; p < b.length / 2; p++) {
            int tmp = a[p];
           b[p] = b[b.length - p - 1];
            b[b.length - p - 1] = tmp;

        }
        System.out.print("Список без повторов в обратном порядке:");
        System.out.println(Arrays.toString(b));
    }
}

