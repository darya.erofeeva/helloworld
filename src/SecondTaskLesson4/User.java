package SecondTaskLesson4;

abstract class User implements Person {

    private String name;

    public String getName() { return name; }

    public User(String name){
        this.name=name;
    }

}

