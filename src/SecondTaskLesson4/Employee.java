package SecondTaskLesson4;

public class Employee extends User {
    private String company;

    public Employee(String name, String company) {
        super(name);
        this.company = company;
    }
    public String getCompany() { return company; }
    @Override
    public void users() {

    }
}