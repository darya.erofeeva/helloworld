package SecondTaskLesson4;

public class ContactInformation extends User {
    private String contact;

    public ContactInformation(String name, String contact) {
        super(name);
        this.contact = contact;
    }

    public String getContact() { return contact; }

    @Override
    public void users() {

    }
}
