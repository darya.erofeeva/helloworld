import java.util.*;
public class duplicates {
    public static void main(String[] args) {
        int[] a = { 1, 1, 1, 2, 3, 3 };
        //  удалить дубликаты
        int n = a.length;
        for ( int i = 0, m = 0; i != n; i++, n = m )
        {
            for ( int j = m = i + 1; j != n; j++ )
            {
                if ( a[j] != a[i] )
                {
                    if ( m != j ) a[m] = a[j];
                    m++;
                }
            }
        }
        if ( n != a.length )
        {
            int[] b = new int[n];
            for ( int i = 0; i < n; i++ ) b[i] = a[i];
            a = b;
        }

//  Эл-ты массива в обратном порядке
        for(int p = 0; p < a.length / 2; p++)
        {
            int tmp = a[p];
            a[p] = a[a.length - p - 1];
            a[a.length - p - 1] = tmp;

        }
        System.out.println(Arrays.toString(a));
}}

