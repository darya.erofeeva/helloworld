package ThirdTaskLesson4;

public class CircleFigure extends Geometry {
    float p=3.14f;//pi
    float r;//radius

    @Override
    void figure(float side) {
        this.r=side;
        float s1 = 2*p*r; //square
        float p1 = s1*r; //perimeter
        System.out.println("Perimeter of circle - "+" "+p1);
        System.out.println("Square of square - "+" "+s1);
    }
}
