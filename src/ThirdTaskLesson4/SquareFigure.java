package ThirdTaskLesson4;

public class SquareFigure extends Geometry {
    float a;

    @Override
    void figure(float side) {
        this.a = side;
        float p2 = 4*side;
        float s2 = side*side;
        System.out.println("Perimeter of square - "+" "+p2);
        System.out.println("Square of square - "+" " +s2);
    }
}
