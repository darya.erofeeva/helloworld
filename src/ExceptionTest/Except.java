package ExceptionTest;


public class Except {

    public static void test1() {
        String st = null;
        try {
            System.out.println(st.length());
        } catch (NullPointerException a) {
            a.printStackTrace();

        }
    }

    public static void test2() {
        int a[] = {10, 20};
        try {
            a[2] = 50;
        } catch (ArrayIndexOutOfBoundsException b) {
            b.printStackTrace();

        }

    }
    public static void test3() {
        String str = "abc";
        try {
            int i = Integer.parseInt(str);
        } catch (NumberFormatException c) {
            c.printStackTrace();

        }

    }
    public static void test4() {
        try {
            Class.forName("qa.qa");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
        }

    }
    public static void test5() {
        try {
            String test = "qaqa";
            Object test2 = test;
            Integer test3 = (Integer) test2;

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }
    public static void test6() {
        try {
            char[] k = Character.toChars(-2);
        }
        catch (IllegalArgumentException f){
            f.printStackTrace();
        }
    }
    public static void test7() {
        try {
           List<Object> list = Collections.emptyList();
            list.add(new Object());
        }
        catch (UnsupportedOperationException g){
            g.printStackTrace();
        }
    }
    public static void test8() {
        try {
            throw new IllegalStateException("Tested by QA");
        }
        catch (IllegalStateException e){
            e.printStackTrace();
        }
    }
    public void test9()
    {
        this.test9();
    }

    public void test10()
    {
        this.test9();
    }

    public static void main(String[] args) {
    Except m = new Except();
        m.test1();
        m.test2();
        m.test3();
        m.test4();
        m.test5();
        m.test6();
        m.test7();
        m.test8();
        m.test9();
    }
}
