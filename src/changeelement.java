import java.util.ArrayList;

public class changeelement {
    public static void main(String[] args) {
        ArrayList<String> elements = new ArrayList<String>();
        // добавим в список ряд элементов
        elements.add("test1");
        elements.add("test2");
        elements.add("test3");
        elements.add("test4");
        elements.add("test5");
        elements.add("test6");
        elements.add("test7");
        elements.add("test8");
        elements.add("test9");
        elements.add("test10");
        System.out.println("3rd element " + elements.get(2));// получаем 3-й объект
        elements.set(2,"test8"); // установка нового значения для 3-го объекта
        System.out.println("New element " + elements.get(2));
    }
}
